public class Defensa extends Jugador {

    private int nivelResistencia;

    public Defensa(String nombreEquipo, String colorUniforme, String nombre, String apellido, String altura, String edad, String dorsal, int valorAccion, int nivelResistencia) {
        super(nombreEquipo, colorUniforme, nombre, apellido, altura, edad, dorsal, valorAccion);
        this.nivelResistencia = nivelResistencia;
    }

    public int getNivelResistencia() {
        return nivelResistencia;
    }

    public void setNivelResistencia(int nivelResistencia) {
        this.nivelResistencia = nivelResistencia;
    }


    @Override
    public String jugada() {
        return super.jugada().toString() +
        "la tapa no la tapa";
    }
}
