public class Persona extends Equipo{

    private String nombre;
    private String apellido;
    private String Altura;
    private String Edad;

    public Persona(String nombreEquipo, String colorUniforme, String nombre, String apellido, String altura, String edad) {
        super(nombreEquipo, colorUniforme);
        this.nombre = nombre;
        this.apellido = apellido;
        Altura = altura;
        Edad = edad;
    }

    public Persona(String nombreEquipo, String colorUniforme, String nombre, String apellido) {
        super(nombreEquipo, colorUniforme);
        this.nombre = nombre;
        this.apellido = apellido;
    }


    public void respirar(){};

    public void caminar(){};



}
