public class Arquero extends Jugador{

    private int nivelReflejo;

    public Arquero(String nombreEquipo, String colorUniforme, String nombre, String apellido, String altura, String edad, String dorsal, int valorAccion, int nivelReflejo) {
        super(nombreEquipo, colorUniforme, nombre, apellido, altura, edad, dorsal, valorAccion);
        this.nivelReflejo = nivelReflejo;
    }

    public int getNivelReflejo() {
        return nivelReflejo;
    }

    public void setNivelReflejo(int nivelReflejo) {
        this.nivelReflejo = nivelReflejo;
    }


    @Override
    public String jugada() {
        return super.jugada().toString() +
                "Tapa NoTapa";
    }
}
