public class MainTorneo {

    public void torneo(){



    }
    public static void main   (String [] args ){
        //Inicio Torneo

        //Se definen 6 equipos
        Equipo[] listaEquipos = new Equipo[5];
        listaEquipos[0] = new Equipo("Juan Valdez","Vinotinto");
        listaEquipos[1] = new Equipo("Los Sayas","Rojo");
        listaEquipos[2] = new Equipo("Las Farc","Blanco");
        listaEquipos[3] = new Equipo("Duque tu papa","Amarillo con rojo");
        listaEquipos[4] = new Equipo("Uninpayu","Naranja");
        listaEquipos[5] = new Equipo("Team Morris","Azul Claro");


        //Se crean 2 volantes por equipo
        Volante[] volantesEquipos = new Volante[11];
        volantesEquipos[0] = new Volante(listaEquipos[0].getNombreEquipo(), listaEquipos[0].getColorUniforme(),"Camilo","Zambrano","1,31","21","10",0,70);
        volantesEquipos[1] = new Volante(listaEquipos[0].getNombreEquipo(), listaEquipos[0].getColorUniforme(),"Juan","Cubides","1,60","29","9",1,100);
        volantesEquipos[2] = new Volante(listaEquipos[1].getNombreEquipo(), listaEquipos[1].getColorUniforme(),"Andres","Camargo","1,76","42","10",0,70);
        volantesEquipos[3] = new Volante(listaEquipos[1].getNombreEquipo(), listaEquipos[1].getColorUniforme(),"Sebastian","Naranjo","1,24","21","9",1,100);
        volantesEquipos[4] = new Volante(listaEquipos[2].getNombreEquipo(), listaEquipos[2].getColorUniforme(),"Victor","Santafe","1,65","34","7",0,70);
        volantesEquipos[5] = new Volante(listaEquipos[2].getNombreEquipo(), listaEquipos[2].getColorUniforme(),"Cleier","Mosquera","1,87","39","4",1,100);
        volantesEquipos[6] = new Volante(listaEquipos[3].getNombreEquipo(), listaEquipos[3].getColorUniforme(),"Ancizar","Gomez","1,34","23","10",0,70);
        volantesEquipos[7] = new Volante(listaEquipos[3].getNombreEquipo(), listaEquipos[3].getColorUniforme(),"Frank","Ceballos","1,63","39","7",1,100);
        volantesEquipos[8] = new Volante(listaEquipos[4].getNombreEquipo(), listaEquipos[4].getColorUniforme(),"James","Melo","1,75","23","9",0,70);
        volantesEquipos[9] = new Volante(listaEquipos[4].getNombreEquipo(), listaEquipos[4].getColorUniforme(),"Jhon","Perez","1,72","38","7",1,100);
        volantesEquipos[10] = new Volante(listaEquipos[5].getNombreEquipo(), listaEquipos[5].getColorUniforme(),"Juan","Santos","1,32","21","0",0,70);
        volantesEquipos[11] = new Volante(listaEquipos[5].getNombreEquipo(), listaEquipos[5].getColorUniforme(),"Carlos","Iglesias","1,63","29","9",1,100);

        //Se crean 2 defensas por equipo
        Defensa[] defensasEquipos = new Defensa[11];
        defensasEquipos[0] = new Defensa(listaEquipos[0].getNombreEquipo(), listaEquipos[0].getColorUniforme(),"Juan", "Cudriz","1,61","12","21",0,1);
        defensasEquipos[1] = new Defensa(listaEquipos[0].getNombreEquipo(), listaEquipos[0].getColorUniforme(),"Juan", "Cudriz","1,61","12","21",0,1);
        defensasEquipos[2] = new Defensa(listaEquipos[1].getNombreEquipo(), listaEquipos[1].getColorUniforme(),"Juan", "Cudriz","1,61","12","21",0,1);
        defensasEquipos[3] = new Defensa(listaEquipos[1].getNombreEquipo(), listaEquipos[1].getColorUniforme(),"Juan", "Cudriz","1,61","12","21",0,1);
        defensasEquipos[4] = new Defensa(listaEquipos[2].getNombreEquipo(), listaEquipos[2].getColorUniforme(),"Juan", "Cudriz","1,61","12","21",0,1);
        defensasEquipos[5] = new Defensa(listaEquipos[2].getNombreEquipo(), listaEquipos[2].getColorUniforme(),"Juan", "Cudriz","1,61","12","21",0,1);
        defensasEquipos[6] = new Defensa(listaEquipos[3].getNombreEquipo(), listaEquipos[3].getColorUniforme(),"Juan", "Cudriz","1,61","12","21",0,1);
        defensasEquipos[7] = new Defensa(listaEquipos[3].getNombreEquipo(), listaEquipos[3].getColorUniforme(),"Juan", "Cudriz","1,61","12","21",0,1);
        defensasEquipos[8] = new Defensa(listaEquipos[4].getNombreEquipo(), listaEquipos[4].getColorUniforme(),"Juan", "Cudriz","1,61","12","21",0,1);
        defensasEquipos[9] = new Defensa(listaEquipos[4].getNombreEquipo(), listaEquipos[4].getColorUniforme(),"Juan", "Cudriz","1,61","12","21",0,1);
        defensasEquipos[10] = new Defensa(listaEquipos[5].getNombreEquipo(), listaEquipos[5].getColorUniforme(),"Juan", "Cudriz","1,61","12","21",0,1);
        defensasEquipos[11] = new Defensa(listaEquipos[5].getNombreEquipo(), listaEquipos[5].getColorUniforme(),"Juan", "Cudriz","1,61","12","21",0,1);

        //Se crea 1 arquero por equipo
        Arquero[] arqueroEquipos = new Arquero[5];
        arqueroEquipos[0] = new Arquero(listaEquipos[0].getNombreEquipo(),listaEquipos[0].getColorUniforme(),"Camilo", "Sanchez", "1,56","12","12",0,0);
        arqueroEquipos[1] = new Arquero(listaEquipos[1].getNombreEquipo(),listaEquipos[1].getColorUniforme(),"Camilo", "Sanchez", "1,56","12","12",0,0);
        arqueroEquipos[2] = new Arquero(listaEquipos[2].getNombreEquipo(),listaEquipos[2].getColorUniforme(),"Camilo", "Sanchez", "1,56","12","12",0,0);
        arqueroEquipos[3] = new Arquero(listaEquipos[3].getNombreEquipo(),listaEquipos[3].getColorUniforme(),"Camilo", "Sanchez", "1,56","12","12",0,0);
        arqueroEquipos[4] = new Arquero(listaEquipos[4].getNombreEquipo(),listaEquipos[4].getColorUniforme(),"Camilo", "Sanchez", "1,56","12","12",0,0);
        arqueroEquipos[5] = new Arquero(listaEquipos[5].getNombreEquipo(),listaEquipos[5].getColorUniforme(),"Camilo", "Sanchez", "1,56","12","12",0,0);

        //Se crea 1 hincha por equipo
        Hincha[] hinchaEquipos = new Hincha[5];
        hinchaEquipos[0] = new Hincha(listaEquipos[0].getNombreEquipo(),listaEquipos[0].getColorUniforme(),"Juan","Valles");
        hinchaEquipos[1] = new Hincha(listaEquipos[1].getNombreEquipo(),listaEquipos[1].getColorUniforme(),"Juan","Valles");
        hinchaEquipos[2] = new Hincha(listaEquipos[2].getNombreEquipo(),listaEquipos[2].getColorUniforme(),"Juan","Valles");
        hinchaEquipos[3] = new Hincha(listaEquipos[3].getNombreEquipo(),listaEquipos[3].getColorUniforme(),"Juan","Valles");
        hinchaEquipos[4] = new Hincha(listaEquipos[4].getNombreEquipo(),listaEquipos[4].getColorUniforme(),"Juan","Valles");
        hinchaEquipos[5] = new Hincha(listaEquipos[5].getNombreEquipo(),listaEquipos[5].getColorUniforme(),"Juan","Valles");

        //Se crea 1 Entrenador por equipo
        Entrenador[] entrenadorEquipos = new Entrenador[5];
        entrenadorEquipos[0] = new Entrenador(listaEquipos[0].getNombreEquipo(),listaEquipos[0].getColorUniforme(),"Pep","Guardiola",2);
        entrenadorEquipos[1] = new Entrenador(listaEquipos[1].getNombreEquipo(),listaEquipos[1].getColorUniforme(),"Pep","Guardiola",2);
        entrenadorEquipos[2] = new Entrenador(listaEquipos[2].getNombreEquipo(),listaEquipos[2].getColorUniforme(),"Pep","Guardiola",2);
        entrenadorEquipos[3] = new Entrenador(listaEquipos[3].getNombreEquipo(),listaEquipos[3].getColorUniforme(),"Pep","Guardiola",2);
        entrenadorEquipos[4] = new Entrenador(listaEquipos[4].getNombreEquipo(),listaEquipos[4].getColorUniforme(),"Pep","Guardiola",2);
        entrenadorEquipos[5] = new Entrenador(listaEquipos[5].getNombreEquipo(),listaEquipos[5].getColorUniforme(),"Pep","Guardiola",2);

        }
    }
}
