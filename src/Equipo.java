public class Equipo {

    private String nombreEquipo;
    private String colorUniforme;

    public Equipo(String nombreEquipo, String colorUniforme) {
        this.nombreEquipo = nombreEquipo;
        this.colorUniforme = colorUniforme;
    }

    public String getNombreEquipo() {
        return nombreEquipo;
    }

    public void setNombreEquipo(String nombreEquipo) {
        this.nombreEquipo = nombreEquipo;
    }

    public String getColorUniforme() {
        return colorUniforme;
    }

    public void setColorUniforme(String colorUniforme) {
        this.colorUniforme = colorUniforme;
    }
}
