public class Hincha extends Persona {


    public Hincha(String nombreEquipo, String colorUniforme, String nombre, String apellido) {
        super(nombreEquipo, colorUniforme, nombre, apellido);
    }

    public String alentar(){

        return "Vamos muchachos si se puede";
    }

}
